macro "before3B"{
setBatchMode(true);
//CREAR IMAGENES .FITS

//Abrir la imagen
pathfile_name = File.openDialog("Select WideField Images");
open(pathfile_name);

//Leer la ruta y el folder donde se van a guardar la imagenes
dir = getDirectory(pathfile_name);
folder_name = getString("Folder Name","fits images");
File.makeDirectory(dir+folder_name);
File.makeDirectory(dir+folder_name+"/masks");

//Asignar el nombre de la ventana para saber el nombre del archivo
file_name = getTitle();

//Asignar el nombre que van a tener todas la imagenes .fits
image_name = "img00";
image_directory = dir+"/"+folder_name+"/"+image_name;

selectWindow(file_name);
finishAt = getNumber("Number of frames",300);
run ("Duplicate...", "title=tobmp duplicate range=1-finishAt");
selectWindow("tobmp");
for (cont=1; cont<=finishAt;cont++) {
    saveAs("FITS", image_directory+IJ.pad(cont,5)+".fits");
    run("Next Slice [>]");
}
C = getImageID();
selectWindow(file_name);
run("Close");

//CREAR IMAGEN BMP
selectImage(C);
run("Z Project...", "start=1 stop=300 projection=[Average Intensity]");
B = getImageID();
saveAs("BMP",image_directory+".bmp");
selectImage(C);
run("Close");

//CREAR MASCARAS
width=getWidth();
height=getHeight();	
var radiusx = getNumber("Mask Width Radio",10);
var radiusy = getNumber("Mask Height Radio",10); 
var stp = getNumber("Overlappping",5);
//radiusx = 5;
//radiusy = 5;
//stp = 4;
var i=1;
masks_directory = dir+folder_name+"/masks/";
for(x=1+radiusx;x<width-radiusx;x=x+radiusx*2-stp) {
   for(y=1+radiusy;y<height-radiusy;y=y+radiusy*2-stp){   
       	drawRect(x-radiusx, y-radiusy, radiusx*2, radiusy*2);
       	title = "mask"+i+".bmp";
	fn = masks_directory+title;
	run("Hyperstack...", "title=marks type=8-bit display=Color width="+width+" height="+height+" channels=3 slices=1 frames=1");
	
	R = "v=0";
	G = "v=0";
	B = "v=0"; 
	
	Stack.setChannel(1);
	run("Macro...", "slice code=["+R+"]");
	Stack.setChannel(2);
	run("Macro...", "slice code=["+G+"]");
	Stack.setChannel(3);
	run("Macro...", "slice code=["+B+"]");
	Stack.setDisplayMode("composite");
	run("Stack to RGB");
	setColor(255,255,0);
	fillRect(x-radiusx,y-radiusy,radiusx*2,radiusy*2);
 	saveAs("BMP",fn); 
	selectWindow("marks");run("Close");
 	selectWindow(title);run("Close"); 	
 	i++;
		}
	}  
}
