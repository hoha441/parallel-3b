#!/bin/bash

# Haydee O . Hdez .
# hoha441@gmail . com
# Septiembre 2014
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3.0 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU General Public License     
# along with this program.  If not, see <http://www.gnu.org/licenses/>




#Your locatition will be in the folder with the executable file of 3B(multispot5_headless_200it) and the configure file (multispot5.cfg)

#PATH WITH ALL IMAGES OF THE EXPERIMENT
echo "PATH OF THE IMAGES OF THE EXPERIMENT"
read PATH_DATA

#Get the regular name of the experiment images
echo "Image file regular name"
read NAME_DATA

#PATH WITH ALL IMAGES OF THE EXPERIMENT
echo "PATH OF THE MASKS OF THE EXPERIMENT"
read PATH_MASKS

#Get the regular name of the masks
echo "Mask file regular name"
read NAME_MASK

#Get the number of masks
echo "NUMBER OF MASKS"
read No_MASKS

#Make the folder where results will be save
echo "PATH OF THE DIRECTORY WHERE THE RESULTS WILL BE SAVE"
read PATH_RESULTS

#Make the folder where the console output will be save
echo "PATH OF THE DIRECTORY WHERE THE CONSOLE OUTPUT WILL BE SAVE"
read PATH_OUTPUT

#Get blur.mu initial number of spots(replace for calcutation according the size of the mask)
echo "BLUR MU"
read BLUR_MU
echo "Spots number"
read No_SPOTS

#Get the number of cpu's in the pc
cpus=$(nproc)

CDIRECTORY=$(pwd)
echo $CDIRECTORY

nameProgram=multispot5_headless_200it

jobsrunning=1
while [ $jobsrunning -le $No_MASKS ];
  do ./$nameProgram --blur.mu $BLUR_MU --placement.uniform.num_spots $No_SPOTS --save_spots $PATH_RESULTS/result_mask$jobsrunning.txt --log_ratios $PATH_MASKS/$NAME_MASK$jobsrunning.bmp $PATH_DATA/$NAME_DATA*.fits > $PATH_OUTPUT/output_mask$jobsrunning.txt &
#echo $jobsrunning;
  sleep 6;
  threads=$(ps -A |grep -i "multispot5_head" | wc -l);
#echo Number of threads out cycle $threads;
  echo $jobsrunning;
  while [ $threads -eq $cpus ]; 
	do sleep 10; 
	threads=$(ps -A |grep -i "multispot5_head" | wc -l);
#echo Threads of the cycle $threads;
  done;
  jobsrunning=$(( $jobsrunning + 1 ));
  done;
wait;

