#!/bin/bash

# job name.
#$ -N Job_Name

### Ask for a core...
#$ -pe thread 1

# There are 400 subregions to analyze
#$ -t 1-400


# Join output and errors in a file
#$ -j y

#PATH=$PATH:/share/apps/multispot5/bin/
PATH=$PATH:/home/username/

# Dynamic libraries for multispot(3B)
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/share/apps/External/libcvd/lib/:/share/apps/External/gvars-3.0/lib/

# Definicio del scratch
SCRATCH=/state/partition1/$USER/${JOB_TASK_ID}/

mkdir -p $SCRATCH

# Lista de los # blocs # mask
LIST=$HOME/path_to/lista-serie-mask.txt

b=$(cat $LIST | head -n $SGE_TASK_ID | tail -n 1| cut -d " " -f 1) 
m=$(cat $LIST | head -n $SGE_TASK_ID | tail -n 1| cut -d " " -f 2)

mask=/path_to/masks/mask${m}.bmp
img=/path_to_images_folder/img000*
result=/path_to_results_folder/result_${b}_${m}.txt

# Ver la opcion que permite utilizar mas cores...

path_to/multispot5_headless --blur.mu 0.146787462461197615315455777817987836897373199462890625 --placement.uniform.num_spots 40 --save_spots $result --log_ratios $mask $img

